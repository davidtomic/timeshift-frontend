export class Employee {

  id: number;
  name: string;
  dateOfBirth: string;
  shiftType: string;
  workplace: string;
}
