import { Timeshift } from './Timeshift';

export class DayModel {

  date: string;
  timeshifts: Timeshift[];
}
