import { Employee } from './Employee';

export class Timeshift {

  id: number;
  employee: Employee;
  timestamp: string;
  logType: string;
  employeeName: string;
}
