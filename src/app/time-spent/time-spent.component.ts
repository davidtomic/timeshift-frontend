import { Component, OnInit } from '@angular/core';
import { Employee } from '../models/Employee';
import { EmployeeService } from '../employees/employee.service';
import { Router } from '@angular/router';
import { TimeshiftService } from '../timeshift/timeshift.service';
import { error } from '@angular/compiler/src/util';
import { DayModel } from '../models/DayModel';

@Component({
  selector: 'app-time-spent',
  templateUrl: './time-spent.component.html',
  styleUrls: ['./time-spent.component.scss']
})
export class TimeSpentComponent implements OnInit {

  allEmployees: Employee[] = [];
  private selectedEmployee: Employee;

  dayHours: DayModel[];

  cols: any[];

  private error: string;

  constructor(
    private employeeService: EmployeeService,
    private timeshiftService: TimeshiftService,
    private router: Router
  ) { }

  ngOnInit() {
    if(localStorage.getItem('loggedIn').includes('false')){
      this.router.navigate(['']);
    }

    this.employeeService.getEmployees().subscribe(
      res => {
        res.body.forEach( employee => {
          employee.dateOfBirth = new Date(employee.dateOfBirth).toLocaleString('hr-HR')
        })
        this.allEmployees = res.body as Employee[];
      },
      error => this.error = error
    );

    this.cols = [
      {field: 'name', header: 'Name'},
      {field: 'dateOfBirth', header: 'Date of birth'},
      {field: 'workplace', header: 'Work place'}
    ];
  }

  onRowSelect(event) {
    this.selectedEmployee = this.cloneEmployee(event.data);

    this.timeshiftService.getTimeshiftForEmployeeInSelectedTime(this.selectedEmployee.id, 0, 2598716000000).subscribe(res => {
      this.dayHours = res.body as DayModel[];
    }, error => {
      this.error = error;
    })
    ;
  }

  cloneEmployee = (t: Employee): Employee => {
    let employee = new Employee();
    for (let prop in t) {
      employee[prop] = t[prop];
    }
    return employee;
  };
}
