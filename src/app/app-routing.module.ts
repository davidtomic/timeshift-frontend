import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { EmployeesComponent } from './employees/employees.component';
import { TimeshiftsComponent } from './timeshift/timeshifts.component';
import { EmployeeTimeshiftsComponent } from './single-employee-timeshifts/employee-timeshifts.component';
import { CalendarComponent } from './calendar/calendar.component';
import { TimeSpentComponent } from './time-spent/time-spent.component';

const routes: Routes = [
  { path: 'timeshifts',
    component: TimeshiftsComponent
  },
  {
    path: 'employees',
    component: EmployeesComponent
  },
  { path: '',
    component: LoginComponent
  },
  {
    path: 'employee-timeshifts/:id',
    component: EmployeeTimeshiftsComponent
  },
  {
    path: 'calendar',
    component: CalendarComponent
  },
  {
    path: 'timespent',
    component: TimeSpentComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
