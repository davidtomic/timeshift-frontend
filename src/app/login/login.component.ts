import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  returnUrl: string;

  private error: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private loginService: LoginService
  ) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.returnUrl = '/timeshifts';

    if(localStorage.getItem('loggedIn').includes('true')){
      this.router.navigate([this.returnUrl]);
    }
  }

  onSubmit() {
    if(!this.loading) {
      this.loading = true;
      this.loginService.login(this.loginForm.get('username').value, this.loginForm.get('password').value).subscribe(res => {
        if(res){
          localStorage.setItem("loggedIn", "true");
          this.router.navigate([this.returnUrl]);
          this.loading = false;
        } else {
          localStorage.setItem("loggedIn", "false");
          alert('Wrong username and password');
          this.loading = false;
        }
      }, onerror =>{
        localStorage.setItem("loggedIn", "false");
        this.error = onerror;
        alert(this.error);
        this.loading = false;
      });
    }
  }
}
