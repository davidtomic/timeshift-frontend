import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private BASE_URL = 'http://localhost:8080/api/v1/users';

  constructor(private http: HttpClient) {}

  createUser(user: User): Observable<Object> {
    return this.http.post(this.BASE_URL + '/create', user, {observe: 'response' });
  }

  login(username, password: string): Observable<boolean>{
    let user: User = new User();
    user.username = username;
    user.password = password;

    return this.http.post<boolean>(this.BASE_URL + '/login', user, {observe: 'body' });
  }
}
