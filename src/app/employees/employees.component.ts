import { Component, OnInit } from '@angular/core';
import { EmployeeService } from './employee.service';
import { Employee } from '../models/Employee';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {

  selectedEmployee: Employee;

  employee: Employee = null;

  allEmployees: Employee[] = [];

  displayDialog: boolean;

  newEmployee: boolean;

  cols: any[];

  private error: string;

  constructor(
    private employeeService: EmployeeService,
    private router: Router
  ) {
  }

  ngOnInit() {
    if(localStorage.getItem('loggedIn').includes('false')){
      this.router.navigate(['']);
    }

    this.employeeService.getEmployees().subscribe(
      res => {
        res.body.forEach( employee => {
          employee.dateOfBirth = new Date(employee.dateOfBirth).toLocaleString('hr-HR')
        })
        this.allEmployees = res.body as Employee[];
      },
      error => this.error = error
    );

    this.cols = [
      {field: 'id', header: 'Id'},
      {field: 'name', header: 'Name'},
      {field: 'dateOfBirth', header: 'Date of birth'},
      {field: 'shiftType', header: 'Shift type'},
      {field: 'workplace', header: 'Work place'}
    ];
  }

  showDialogToAdd() {
    this.newEmployee = true;
    this.employee = new Employee();
    this.displayDialog = true;
  }

  save() {
    if (this.newEmployee) {
      this.employeeService.createEmployee(this.employee).subscribe(
        (res: HttpResponse<Employee>) => {
          res.body.dateOfBirth = new Date(res.body.dateOfBirth).toLocaleString('hr-HR')
          this.allEmployees.push(res.body as Employee);
        },
        error1 => {
          this.error = error1;
        }
      );
    } else {
      this.employeeService.updateEmployee(this.employee).subscribe(
        (res: HttpResponse<Employee>) => {
          this.allEmployees[this.allEmployees.indexOf(this.selectedEmployee)] = res.body as Employee;
        },
        res => {
          this.error = res;
        }
      );
    }
    this.employee = null;
    this.displayDialog = false;
  }

  delete() {
    if (this.employee && this.employee.id) {
      this.employeeService.deleteEmployee(this.employee).subscribe(
        (res: HttpResponse<string>) => {
          let index = this.allEmployees.indexOf(this.selectedEmployee);
          this.allEmployees = this.allEmployees.filter((val, i) => i != index);
          this.employee = null;
          this.displayDialog = false;
        },
        (res: HttpErrorResponse) => {
          alert('There was an error deleting employee.');
        }
      );
    }
    this.employee = null;
    this.displayDialog = false;
  }

  onRowSelect(event) {
    this.employee = this.cloneEmployee(event.data);
    this.newEmployee = false;
    this.displayDialog = true;
  }

  cloneEmployee = (t: Employee): Employee => {
    let employee = new Employee();
    for (let prop in t) {
      employee[prop] = t[prop];
    }
    return employee;
  };

  showTimeshifts(employee: Employee) {
    this.router.navigate(['employee-timeshifts', employee.id]);
  }
}
