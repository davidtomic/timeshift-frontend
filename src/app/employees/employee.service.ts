import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Employee } from '../models/Employee';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private BASE_URL = 'http://localhost:8080/api/v1';

  constructor(private http: HttpClient) {}

  createEmployee(employee: Employee): Observable<Object> {
    return this.http.post(this.BASE_URL + '/employee/create', employee, {observe: 'response' });
  }

  updateEmployee(employee: Employee): Observable<Object> {
    return this.http.put(this.BASE_URL + '/employee/update', employee, {observe: 'response' });
  }

  deleteEmployee(employee: Employee) {
    return this.http.get(this.BASE_URL + '/employee/delete/' + employee.id, {observe: 'response' });
  }

  getEmployees(): Observable<HttpResponse<Employee[]>> {
    return this.http.get<Employee[]>(this.BASE_URL + '/employee/findall', {observe: 'response' });
  }

  getEmployeeById(employeeId: any): Observable<HttpResponse<Employee>> {
    return this.http.get<Employee>(this.BASE_URL + '/employee/findById/' + employeeId, {observe: 'response' });
  }
}
