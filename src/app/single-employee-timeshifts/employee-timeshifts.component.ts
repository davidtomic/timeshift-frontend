import { Component, OnInit } from '@angular/core';
import { Timeshift } from '../models/Timeshift';
import { TimeshiftService } from '../timeshift/timeshift.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Employee } from '../models/Employee';
import { EmployeeService } from '../employees/employee.service';

@Component({
  selector: 'app-employee-timeshifts',
  templateUrl: './employee-timeshifts.component.html',
  styleUrls: ['./employee-timeshifts.component.scss']
})
export class EmployeeTimeshiftsComponent implements OnInit {

  selectedTimeshift: Timeshift;

  timeshift: Timeshift = null;

  timeshifts: Timeshift[] = [];

  displayDialog: boolean;

  newTimeshift: boolean;

  employee: Employee;

  employeeTimeshifts: string;

  cols: any[];

  private error: string;

  constructor(
    private timeshiftService: TimeshiftService,
    private employeeService: EmployeeService,
    private activatedRouter: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit() {
    if(localStorage.getItem('loggedIn').includes('false')){
      this.router.navigate(['']);
    }

    this.activatedRouter.params.subscribe(
      (params: Params) => {
        this.employeeService.getEmployeeById(params['id']).subscribe(
          res => {
            this.employee = res.body as Employee;
            this.employeeTimeshifts = this.employee.name + ' timeshifts';
          },
          error => {
            this.error = error;
          }
        );
        this.timeshiftService.getTimeshiftForEmployee(params['id']).subscribe(
          res => {
            res.body.forEach( timeshift => {
              var numberTimestamp: number = +timeshift.timestamp;
              timeshift.timestamp = new Date(numberTimestamp).toLocaleString('hr-HR')
            })
            this.timeshifts = res.body as Timeshift[]
          },
          error => {
            this.error = error;
          }
        );
      });

    this.cols = [
      { field: 'id', header: 'ID' },
      { field: 'logType', header: 'Log type' },
      { field: 'timestamp', header: 'Timestamp' }
    ];
  }

  showDialogToAdd() {
    this.newTimeshift = true;
    this.timeshift = new Timeshift();
    this.displayDialog = true;
  }

  save() {
    if (this.newTimeshift) {
      this.timeshift.employee = this.employee;
      let date = new Date(this.timeshift.timestamp);
      this.timeshift.timestamp = date.getTime().toString();
      this.timeshiftService.createTimeshift(this.timeshift).subscribe(
        (res: HttpResponse<Timeshift>) => {
          var numberTimestamp: number = +res.body.timestamp;
          res.body.timestamp = new Date(numberTimestamp).toLocaleString('hr-HR')
          this.timeshifts.push(res.body as Timeshift);
        },
        error1 => {
          this.error = error1;
        }
      );
    }
    else
      this.timeshiftService.updateTimeshift(this.timeshift).subscribe(
        (res: HttpResponse<Timeshift>) => {
          this.timeshifts[this.timeshifts.indexOf(this.selectedTimeshift)] = res.body as Timeshift;
        },
        error1 => {
          this.error = error1;
        }
      );
    this.timeshift = null;
    this.displayDialog = false;
  }

  delete() {
    if(this.timeshift && this.timeshift.id) {
      this.timeshiftService.deleteTimeshift(this.timeshift).subscribe(
        (res: HttpResponse<string>) => {
          let index = this.timeshifts.indexOf(this.selectedTimeshift);
          this.timeshifts = this.timeshifts.filter((val, i) => i != index);
          this.timeshift = null;
          this.displayDialog = false;
        },
        (res: HttpErrorResponse) => {
          alert('There was an error deleting timeshift.');
        }
      );
    }
    this.timeshift = null;
    this.displayDialog = false;
  }

  onRowSelect(event) {
    this.newTimeshift = false;
    this.timeshift = this.cloneTimeshift(event.data);
    this.displayDialog = true;
  }

  cloneTimeshift = (t: Timeshift): Timeshift => {
    let timeshift = new Timeshift();
    for (let prop in t) {
      timeshift[prop] = t[prop];
    }
    return timeshift;
  };

}
