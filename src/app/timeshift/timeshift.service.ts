import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Timeshift } from '../models/Timeshift';
import { DayModel } from '../models/DayModel';

@Injectable({
  providedIn: 'root'
})
export class TimeshiftService {

  private BASE_URL = 'http://localhost:8080/api/v1';

  constructor(private http: HttpClient) {}

  createTimeshift(timeshift: Timeshift): Observable<Object>{
    return this.http.post(this.BASE_URL + '/timeshift/create', timeshift, {observe: 'response' });
  }

  updateTimeshift(timeshift: Timeshift): Observable<Object>{
    return this.http.put(this.BASE_URL + '/timeshift/update', timeshift, {observe: 'response' });
  }

  deleteTimeshift(timeshift: Timeshift){
    return this.http.get(this.BASE_URL + '/timeshift/delete/' + timeshift.id, {observe: 'response' });
  }

  getTimeshifts(): Observable<HttpResponse<Timeshift[]>>{
    return this.http.get<Timeshift[]>(this.BASE_URL + '/timeshift/findall', {observe: 'response' });
  }

  getCalendarTimeshifts(): Observable<HttpResponse<any[]>>{
    return this.http.get<any[]>(this.BASE_URL + '/timeshift/calendarModel', {observe: 'response' });
  }

  getTimeshiftForEmployee(employee: number): Observable<HttpResponse<Timeshift[]>>{
    return this.http.get<Timeshift[]>(this.BASE_URL + '/timeshift/findByEmployeeId/' + employee, {observe: 'response' });
  }

  getTimeshiftForEmployeeInSelectedTime(employee: number, startTime: number, endTime: number): Observable<HttpResponse<DayModel[]>>{
    return this.http.get<DayModel[]>(this.BASE_URL + '/timeshift/employeeHours/' + employee + '/' + startTime + '/' + endTime, {observe: 'response' });
  }

  private onError(res: HttpErrorResponse) {}
}
