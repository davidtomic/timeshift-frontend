import { Component, OnInit } from '@angular/core';
import { Timeshift } from '../models/Timeshift';
import { TimeshiftService } from './timeshift.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { EmployeeService } from '../employees/employee.service';
import { Employee } from '../models/Employee';
import { Router } from '@angular/router';

@Component({
  selector: 'app-timeshifts',
  templateUrl: './timeshifts.component.html',
  styleUrls: ['./timeshifts.component.scss']
})
export class TimeshiftsComponent implements OnInit {


  selectedTimeshift: Timeshift;
  timeshift: Timeshift = null;
  timeshifts: Timeshift[] = [];
  displayDialog: boolean;
  newTimeshift: boolean;
  selectedEmployee: Employee;
  cols: any[];
  allEmployees: any;

  private error: string;

  constructor(
    private timeshiftService: TimeshiftService,
    private employeeService: EmployeeService,
    private router: Router
  ) { }

  ngOnInit() {
    if(localStorage.getItem('loggedIn').includes('false')){
      this.router.navigate(['']);
    }

    this.timeshiftService.getTimeshifts().subscribe(
      res => {
        res.body.forEach( timeshift => {
          var numberTimestamp: number = +timeshift.timestamp;
          timeshift.timestamp = new Date(numberTimestamp).toLocaleString('hr-HR')
        })
        this.timeshifts = res.body as Timeshift[];
        for(let i=0; i < this.timeshifts.length; i++) {
          this.timeshifts[i].employeeName = this.timeshifts[i].employee.name;
        }
      },
      error => this.error = error
    );

    this.employeeService.getEmployees().subscribe(
      res =>{
        this.allEmployees = res.body as Employee[]
      },
      res => this.error = res
    );

    this.cols = [
      { field: 'id', header: 'ID' },
      { field: 'employeeName', header: 'Employee' },
      { field: 'logType', header: 'Log type' },
      { field: 'timestamp', header: 'Timestamp' }
    ];
  }

  showDialogToAdd() {
    this.newTimeshift = true;
    this.timeshift = new Timeshift();
    this.timeshift.employee = this.allEmployees[0];
    this.displayDialog = true;
  }

  save() {
    if (this.newTimeshift) {
      if(!this.timeshift.employee){
        alert('Please select employee');
      }
      if(!this.timeshift.timestamp){
        alert('Please select date');
      }
      if(!this.timeshift.logType){
        alert('Please select log type');
      }
      let date = new Date(this.timeshift.timestamp);
      this.timeshift.timestamp = date.getTime().toString();
      this.timeshiftService.createTimeshift(this.timeshift).subscribe(
        (res: HttpResponse<Timeshift>) => {
          var numberTimestamp: number = +res.body.timestamp;
          res.body.timestamp = new Date(numberTimestamp).toLocaleString('hr-HR')
          var timeshift = res.body as Timeshift;
          timeshift.employeeName = timeshift.employee.name;
          this.timeshifts.push(timeshift);
        },
        error1 => {
          this.error = error1;
        }
      );
    }
    else
      this.timeshiftService.updateTimeshift(this.timeshift).subscribe(
        (res: HttpResponse<Timeshift>) => {
          this.timeshifts[this.timeshifts.indexOf(this.selectedTimeshift)] = res.body as Timeshift;
        },
        error1 => {
          this.error = error1;
        }
      );
    this.timeshift = null;
    this.displayDialog = false;
  }

  delete() {
    if(this.timeshift && this.timeshift.id) {
      this.timeshiftService.deleteTimeshift(this.timeshift).subscribe(
        (res: HttpResponse<string>) => {
          let index = this.timeshifts.indexOf(this.selectedTimeshift);
          this.timeshifts = this.timeshifts.filter((val, i) => i != index);
          this.timeshift = null;
          this.displayDialog = false;
        },
        (res: HttpErrorResponse) => {
          alert('There was an error deleting timeshift.');
        }
      );
    }
    this.timeshift = null;
    this.displayDialog = false;
  }

  onRowSelect(event) {
    this.newTimeshift = false;
    this.timeshift = this.cloneTimeshift(event.data);
    this.displayDialog = true;
  }

  cloneTimeshift = (t: Timeshift): Timeshift => {
    let timeshift = new Timeshift();
    for (let prop in t) {
      timeshift[prop] = t[prop];
    }
    return timeshift;
  };
}
