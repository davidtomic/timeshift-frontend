import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { TimeshiftsComponent } from './timeshift/timeshifts.component';
import { EmployeesComponent } from './employees/employees.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PanelModule } from 'primeng/panel';
import { ButtonModule } from 'primeng/button';
import { CalendarModule, CheckboxModule, DialogModule, InputTextModule, PaginatorModule, ToolbarModule } from 'primeng/primeng';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TableModule } from 'primeng/table';
import { EmployeeTimeshiftsComponent } from './single-employee-timeshifts/employee-timeshifts.component';
import { HttpClientModule } from '@angular/common/http';
import { DateFormatPipe } from './helpers/DateFormatPipe';
import { CalendarComponent } from './calendar/calendar.component';
import { FullCalendarModule } from 'primeng/fullcalendar';
import { TimeSpentComponent } from './time-spent/time-spent.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TimeshiftsComponent,
    EmployeesComponent,
    ToolbarComponent,
    EmployeeTimeshiftsComponent,
    DateFormatPipe,
    CalendarComponent,
    TimeSpentComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    PanelModule,
    ReactiveFormsModule,
    ButtonModule,
    ToolbarModule,
    TableModule,
    DialogModule,
    CheckboxModule,
    PaginatorModule,
    InputTextModule,
    CalendarModule,
    FullCalendarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
