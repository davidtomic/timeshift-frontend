import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import { Component, OnInit } from '@angular/core';
import { TimeshiftService } from '../timeshift/timeshift.service';
import { Timeshift } from '../models/Timeshift';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  timeshifts: Timeshift[];
  dates: any[];

  options: any;

  constructor(private eventService: TimeshiftService) { }

  ngOnInit() {

    this.eventService.getCalendarTimeshifts().subscribe(timeshifts => {
      this.dates = timeshifts.body;
    });

    var currentDate = new Date();
    var month = currentDate.getUTCMonth() + 1; //months from 1-12
    var fixedMonth = month + "";
    if(month < 10){
      fixedMonth = '0' + month;
    }

    var day = currentDate.getUTCDate();
    var fixedDay = day + "";
    if(day < 10){
      fixedDay = '0' + day;
    }

    var year = currentDate.getUTCFullYear();
    var dateString = year + "-" + fixedMonth + "-" + fixedDay;

    this.options = {
      plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
      defaultDate: dateString,
      header: {
        left: 'prev,next',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      editable: true
    };
  }

}
